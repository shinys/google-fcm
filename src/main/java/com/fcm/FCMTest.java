package com.fcm;


//https://firebase.google.com/support/release-notes/admin/java?hl=ko
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author younsshin
 */
public class FCMTest {
    
    static GoogleCredential googleCredential;
    
    static{
        try {
            googleCredential = GoogleCredential
                    .fromStream(new FileInputStream("/Users/user/Documents/project/google-fcm/seedn-1470103259488-firebase-adminsdk-8gjbw-b7d55713c7.json"))
                    //https://cloud.google.com/compute/docs/access/create-enable-service-accounts-for-instances#using 구글앱앤진 사용 시
                    //http://theeye.pe.kr/archives/tag/googlecredential
                    //https://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages/send?hl=ko <-- Scope 정보 여기 나와 있음
                    .createScoped(Arrays.asList("https://www.googleapis.com/auth/cloud-platform","https://www.googleapis.com/auth/firebase.messaging"));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FCMTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FCMTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    //https://firebase.google.com/docs/cloud-messaging/auth-server?hl=ko
    private static String getAccessToken() throws IOException{        
        googleCredential.refreshToken();
        return googleCredential.getAccessToken();
    }
    
    //https://firebase.google.com/docs/cloud-messaging/concept-options?hl=ko
    private static void sendMessage() throws MalformedURLException, ProtocolException, IOException{
        //https://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages/send?hl=ko
                
        URL url = new URL("https://fcm.googleapis.com/v1/projects/seedn-1470103259488/messages:send");
        //URL url = new URL("https://fcm.googleapis.com/fcm/send");
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setRequestProperty("Authorization", "Bearer " + getAccessToken());
        httpURLConnection.setRequestProperty("Content-Type", "application/json; UTF-8");
        httpURLConnection.setRequestProperty("User-Agent", "SeedN Rest Client");
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setDoInput(true);
        httpURLConnection.setRequestMethod("POST");
        
        
        BufferedWriter bw = new BufferedWriter( new OutputStreamWriter( httpURLConnection.getOutputStream(), "utf-8") );
        // 메세지 포맷 https://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages?hl=ko#Notification
        // https://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages/send
        bw.write("{\n" +
                "    \"validate_only\":true,      \n" +
                "    \"message\": {\n" +
                "        \"token\":\"푸시 수신받을 기기 토큰 값\", \n" +
                "        \"notification\" : {\n" +
                "            \"title\" : \"제목넣기 \", \n" +
                "            \"body\" : \"내용 넣기\"\n" +
                "        }, \n" +
                "        \"data\" : {  \n" +
                "            \"group\": \"알림 발생 마스터 속한 그룹이름\",\n" +
                "            \"pin_num\" : \"마스터 pin_num\",\n" +
                "            \"sensor\": \"센서id\",\n" +
                "            \"it\": \"26\",\n" +
                "            \"ih\": \"60\"\n" +
                "        }  \n" +
                "    } \n" +
                "}");
        bw.flush();
        bw.close();
        
        int httpcode = httpURLConnection.getResponseCode();
        System.out.println("http code :"+httpcode);
        if( httpcode == 200 ){
            BufferedReader br = new BufferedReader( new InputStreamReader( httpURLConnection.getInputStream(), "utf-8") );
            String s= null;
            StringBuilder b = new StringBuilder();
            while((s=br.readLine())!=null){
                b.append(s);
            }
            System.out.println(b.toString());
            br.close();
        }else{
            BufferedReader er = new BufferedReader( new InputStreamReader( httpURLConnection.getErrorStream(), "utf-8") );
            String s= null;
            StringBuilder b = new StringBuilder();
            while((s=er.readLine())!=null){
                b.append(s);
            }
            //{  "error": {    "code": 400,    "message": "Request contains an invalid argument.",    "status": "INVALID_ARGUMENT",    "details": [      {        "@type": "type.googleapis.com/google.firebase.fcm.v1.FcmError",        "errorCode": "INVALID_ARGUMENT"      },      {        "@type": "type.googleapis.com/google.rpc.BadRequest",        "fieldViolations": [          {            "field": "message.token",            "description": "Invalid registration token"          }        ]      }    ]  }}
            System.out.println(b.toString());
            er.close();
        }
        httpURLConnection.disconnect();
    }
    
    public static void main(String[] args) {
        try{
            FCMTest.sendMessage();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
